package projet;

public class Calculatrice {

public Calculatrice(){}

	/**
	 * return the addition of two int
	 * @param a an integer argument.
	 * @param b an integer argument.
	 * @return The results of the addition
	 */
	public int Addition(int a,int b) {
		return a+b;
	}
	
	/**
     * return the addition of two int
	 * @param a an integer argument.
	 * @param b an integer argument.
	 * @return The results of the soustraction
     */
	public int Soustraction(int a, int b) {
		return a-b;
	}
	
	/**
     * return the multiplication of two int
	 * @param a an integer argument.
	 * @param b an integer argument.
	 * @return The results of the multiplication
     */
	public int Multiplication(int a, int b) {
		return a * b;
	}
	
	/**
     * return the division of two int
     * @param a an integer argument.
     * @param b an integer argument.
     * @return The result of the division
     * @throws DividedByZeroException if you try to divide by zero
     */
	public float Division(int a,int b) throws DividedByZeroException {
		if(b == 0)
			throw new DividedByZeroException("La division par zero n'est pas autorisee");
		return (float)a/(float)b;
	}

}
