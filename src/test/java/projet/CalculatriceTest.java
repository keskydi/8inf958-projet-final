package projet;


import static org.junit.Assert.assertEquals;

import org.junit.*;

public class CalculatriceTest {

	@Test(expected=DividedByZeroException.class)
	public void testDividedByZero() throws CalculatriceException {
		Calculatrice calc = new Calculatrice();
		calc.Division(10, 0);
	}
	
	
	@Test
	public void testDivision() {
		Calculatrice calc = new Calculatrice();
		try {
			assertEquals(1.25, calc.Division(5, 4),0);
		} catch (DividedByZeroException e) {
			e.printStackTrace();
		}
	}
	
	@Test
	public void testAddition() {
		Calculatrice calc = new Calculatrice();
		assertEquals(9, calc.Addition(5, 4));
	}
	
	@Test
	public void testSoustration() {
		Calculatrice calc = new Calculatrice();
		assertEquals(1, calc.Soustraction(5, 4));
	}
	
	@Test
	public void testMultiplication() {
		Calculatrice calc = new Calculatrice();
		assertEquals(20, calc.Multiplication(5, 4));
	}

}
